#include <PID_v1.h>

// Define os pinos
const int sensorPin = A0;      // Pino analógico para o sensor de temperatura LM35
const int setpointPin = A4;    // Pino analógico para o setpoint
const int kpPin = A3;          // Pino analógico para o ganho proporcional (kp)
const int kiPin = A2;          // Pino analógico para o ganho integral (ki)
const int kdPin = A1;          // Pino analógico para o ganho derivativo (kd)
const int lampPin = 9;         // Pino PWM para a lâmpada incandescente

// Variáveis para armazenar os valores lidos dos potenciômetros
double setpoint, kp, ki, kd;

// Variáveis para o PID
double input, output, temp;
PID myPID(&input, &output, &setpoint, kp, ki, kd, DIRECT);

void setup() {
  // Inicializa a comunicação serial
  Serial.begin(9600);

  // Configuração dos pinos
  pinMode(lampPin, OUTPUT);

  // Define os limites do PID
  myPID.SetMode(AUTOMATIC);
  myPID.SetOutputLimits(0, 255);
}

void loop() {
  // Lê os valores dos potenciômetros
  setpoint = analogRead(setpointPin);
  kp = analogRead(kpPin) / 100.0; // Ajuste para obter um valor adequado
  ki = analogRead(kiPin) / 100.0; // Ajuste para obter um valor adequado
  kd = analogRead(kdPin) / 100.0; // Ajuste para obter um valor adequado

  // Lê a temperatura do sensor LM35
  int sensorValue = analogRead(sensorPin);
  temp = (sensorValue * 5.0 / 1023) * 100.0; // Converte para graus Celsius

  // Define o setpoint do PID
  myPID.SetSetpoint(setpoint);

  // Define a entrada do PID como a temperatura lida pelo sensor
  input = temp;

  // Calcula a saída do PID
  myPID.Compute();

  // Atualiza a saída do PWM para controlar a lâmpada
  analogWrite(lampPin, output);

  // Imprime informações no monitor serial
  Serial.print("Temp: ");
  Serial.print(temp);
  Serial.print(" Setpoint: ");
  Serial.print(setpoint);
  Serial.print(" Output: ");
  Serial.println(output);

  // Aguarda um curto intervalo de tempo
  delay(5000);
}
